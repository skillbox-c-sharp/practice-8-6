﻿using System.Xml.Linq;

namespace Challenge4;
class Program
{
    static void Main(string[] args)
    {
        XElement contact = new XElement("Person");

        Console.Write("Введите ФИО: ");
        string name = Console.ReadLine()!;

        contact.Add(new XAttribute("Name", name));

        Console.Write("Введите улицу: ");
        string street = Console.ReadLine()!;

        contact.Add(new XElement("Address"));
        contact.Element("Address")!.Add(new XElement("Street", street));

        Console.Write("Введите номер дома: ");
        string houseNumber = Console.ReadLine()!;
        contact.Element("Address")!.Add(new XElement("HouseNumber", houseNumber));

        Console.Write("Введите номер квартиры: ");
        string apartmentNumber = Console.ReadLine()!;
        contact.Element("Address")!.Add(new XElement("FlatNumber", apartmentNumber));

        Console.Write("Введите мобильный телефон: ");
        string mobilePhone = Console.ReadLine()!;
        contact.Add(new XElement("Phones"));
        contact.Element("Phones")!.Add(new XElement("MobilePhone", mobilePhone));

        Console.Write("Введите домашний телефон: ");
        string homePhone = Console.ReadLine()!;
        contact.Element("Phones")!.Add(new XElement("FlatPhone", homePhone));


        //XElement contact = new XElement("Person",
        //new XAttribute("name", name),
        //new XElement("Address",
        // new XElement("Street", street),
        // new XElement("HouseNumber", houseNumber),
        // new XElement("FlatNumber", apartmentNumber)),
        //new XElement("Phones",
        // new XElement("MobilePhone", mobilePhone),
        // new XElement("FlatPhone", homePhone)));

        contact.Save("contact.xml");

        Console.WriteLine("Контакт сохранен в файле contact.xml");

    }
}

