﻿namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        List<int> numbers = new List<int>();
        int min = 25;
        int max = 50;

        numbers = SetList();
        PrintList(numbers);

        Console.WriteLine();

        numbers = RemoveNumbers(numbers, min, max);

        PrintList(numbers);

        Console.ReadKey();
    }

    static List<int> SetList()
    {
        List<int> list = new List<int>();
        Random rand = new Random();

        for (int i = 0; i < 100; i++)
        {
            list.Add(rand.Next(0, 101));
        }

        return list;
    }

    static List<int> RemoveNumbers(List<int> list, int min, int max)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] > min && list[i] < max)
            {
                list.RemoveAt(i);
                i--;
            }
        }
        return list;
    }

    static void PrintList(List<int> list)
    {
        foreach (int e in list) Console.Write($"{e} ");
    }
}

