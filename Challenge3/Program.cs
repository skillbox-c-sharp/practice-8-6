﻿namespace Challenge3;
class Program
{
    static void Main(string[] args)
    {
        HashSet<int> uniqueNumbers = new HashSet<int>();
        uniqueNumbers = SetHash();

        foreach (var e in uniqueNumbers)
        {
            Console.Write($"{e} ");
        }

        Console.ReadKey();
    }

    static HashSet<int> SetHash()
    {
        HashSet<int> tempUniqueNumbers = new HashSet<int>();
        do
        {
            Console.Write("Введите целое число: ");
            string numberStringIn = Console.ReadLine()!;
            if (numberStringIn.Equals("")) break;
            int number;
            if (!int.TryParse(numberStringIn, out number))
            {
                Console.WriteLine("Необходимо ввести целое число!");
                continue;
            }
            else
            {
                if (tempUniqueNumbers.Contains(number))
                {
                    Console.WriteLine("Такое число уже существует в коллекции!");
                }
                else
                {
                    tempUniqueNumbers.Add(number);
                    Console.WriteLine("Число успешно добавлено!");
                }
            }
        } while (true);
        return tempUniqueNumbers;
    }
}

