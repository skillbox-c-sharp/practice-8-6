﻿namespace Challenge2;
class Program
{
    static void Main(string[] args)
    {
        Dictionary<string, string> phoneBook = new Dictionary<string, string>();
        phoneBook = SetPhones();
        PrintPhones(phoneBook);
        SearchOwner(phoneBook);

        Console.ReadKey();
    }

    static Dictionary<string, string> SetPhones()
    {
        string phone = "";
        string fullName = "";
        Dictionary<string, string> tempPhoneBook = new Dictionary<string, string>();

        do
        {
            Console.Write("Введите номер телефона: ");
            phone = Console.ReadLine()!;
            if (phone.Equals("")) break;
            Console.Write("Введите ФИО владельца телефона: ");
            fullName = Console.ReadLine()!;
            if (fullName.Equals("")) break;
            if (!tempPhoneBook.TryAdd(phone, fullName)) Console.WriteLine("Номера телефонов не должны повторяться!");
        } while (true);

        return tempPhoneBook;
    }

    static void PrintPhones(Dictionary<string, string> dictionary)
    {
        foreach (KeyValuePair<string, string> e in dictionary)
        {
            Console.WriteLine($"Телефон: {e.Key} ФИО: {e.Value}");
        }
    }

    static void SearchOwner(Dictionary<string, string> dictionary)
    {
        do
        {
            Console.Write("Введите номер телефона для поиска владельца: ");
            string tempPhone = Console.ReadLine()!;
            if (tempPhone.Equals("")) break;
            string value;
            if (dictionary.TryGetValue(tempPhone, out value!)) Console.WriteLine($"ФИО владельца: {value}");
            else Console.WriteLine("Владелец не найден!");
        } while (true);
    }
}
